export const data = [
     {
          name: 'Nguyen A',
          phone: '0396075444',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen B',
          phone: '0168548512',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen C',
          phone: '0484475121',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen D',
          phone: '035847444',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen E',
          phone: '078489562',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen F',
          phone: '022354845',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen G',
          phone: '034788888',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen H',
          phone: '0158745821',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen P',
          phone: '0578741231',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen L',
          phone: '0396075444',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen Q',
          phone: '0147124724',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen W',
          phone: '04457544411',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen R',
          phone: '0424348754',
          avatar: 'https://picsum.photos/200',
     },
     {
          name: 'Nguyen Y',
          phone: '09887545454',
          avatar: 'https://picsum.photos/200',
     },
];
