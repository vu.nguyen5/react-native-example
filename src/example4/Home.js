/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
     View,
     Text,
     FlatList,
     ActivityIndicator,
     Image,
     TouchableOpacity,
} from 'react-native';
import axios from 'axios';
import { SearchInput, Colors } from '@momo-platform/component-kits';
import Detail from './Detail';

export default class Home extends Component {
     constructor(props) {
          super(props);
          this.state = {
               //isLoading: false,
               data: [],
               defaultData: [],
          };
     }

     componentDidMount() {
          this.makeRemoteRequest();
     }

     makeRemoteRequest = () => {
          //const { page, seed, isRefreshing } = this.state;
          //const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
          const url = 'https://randomuser.me/api/';
          //this.setState({ isLoading: true });
          axios.get(url, {
               params: {
                    seed: 1,
                    page: 1,
                    results: 20,
               },
          })
               .then(response => {
                    const {
                         data: { results },
                    } = response;
                    this.setState({
                         data: results,
                         defaultData: results,
                         //isLoading: false,
                    });
               })
               .catch(error => {
                    console.log(error);
               });
     };

     onGoDetail = profile => {
          const { navigator } = this.props;
          navigator.push({ screen: Detail, params: { profile } });
     };

     onChangeText = text => {
          const { defaultData } = this.state;
          if (text === '') {
               return this.setState({ data: defaultData });
          }
          const lowerText = text.toLowerCase();
          const filterContacts = defaultData.filter(
               item =>
                    item.phone.includes(lowerText) ||
                    `${item.name.first} ${item.name.last}`
                         .toLowerCase()
                         .includes(lowerText),
          );
          this.setState({
               data: filterContacts,
          });
     };

     renderHeader = () => (
          <View
               style={{
                    backgroundColor: 'white',
                    padding: 10,
               }}>
               <SearchInput
                    onChangeText={this.onChangeText}
                    style={{
                         borderWidth: 1,
                         borderColor: Colors.borders,
                         backgroundColor: 'transparent',
                         borderRadius: 30,
                    }}
                    placeholder="Nhập tên hoặc số điện thoại"
               />
          </View>
     );

     renderSeparator = () => {
          return <View style={{ height: 1, backgroundColor: '#CED0CE' }} />;
     };

     renderItem = ({ item, index }) => {
          return (
               <TouchableOpacity
                    onPress={() => this.onGoDetail(item)}
                    style={{
                         flexDirection: 'row',
                         alignItems: 'center',
                         paddingVertical: 12,
                         paddingHorizontal: 12,
                    }}>
                    <Image
                         source={{ uri: item.picture.medium }}
                         style={{ height: 35, width: 35, borderRadius: 30 }}
                    />

                    <View style={{ marginLeft: 10 }}>
                         <Text style={{ fontSize: 14 }}>
                              {`${item.name.first} ${item.name.last}`}
                         </Text>
                         <Text style={{ fontSize: 15, color: 'gray' }}>
                              {item.phone}
                         </Text>
                    </View>
               </TouchableOpacity>
          );
     };

     render() {
          const { data } = this.state;
          return (
               <FlatList
                    style={{ flex: 1, backgroundColor: 'white' }}
                    data={data}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.email}
                    ListHeaderComponent={this.renderHeader}
                    ItemSeparatorComponent={this.renderSeparator}
               />
          );
     }
}

// getItemLayout = (data, index) => ({
//     length: 72,
//     offset: 72 * index,
//     index,
// });
// onRefresh={this.handleRefresh}
// refreshing={this.state.isRefreshing}
// onEndReached={this.handleLoadMore}
// onEndReachedThreshold={0.5}
//initialNumToRender={8}
//windowSize={21}
//removeClippedSubviews
//maxToRenderPerBatch
//updateCellsBatchingPeriod
//getItemLayout
//onMomentumScrollBegin={this.onMomentumScrollBegin}
{
     /* <SelectionItem
                    onPress={() => this.onGoDetail(item)}
                    leftIcon={item.picture.medium}
                    leftIconStyle={{ height: 35, width: 35, borderRadius: 30 }}
                    titleStyle={{ fontSize: 14 }}
                    title={`${item.name.first} ${item.name.last}`}
                    body={item.phone}
               /> */
}
//<Line length={width - 60} style={{ alignSelf: 'flex-end' }} />
