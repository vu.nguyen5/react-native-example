import React, { Component } from 'react';
import { Navigation } from '@momo-platform/component-kits';
import Home from './Home';
export default class App extends Component {
     render() {
          return (
               <Navigation
                    screen={Home}
                    options={{
                         title: 'Dạnh bạ',
                         headerLeft: () => {},
                    }}
               />
          );
     }
}
