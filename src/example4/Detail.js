/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View } from 'react-native';
import { Flex, Image, Text } from '@momo-platform/component-kits';

class Detail extends Component {
     constructor(props) {
          super(props);
          this.props.navigator.setOptions({ title: 'Thông tin cá nhân' });
     }

     render() {
          const { params = {} } = this.props;
          const { name, picture, email, gender, phone } = params?.profile || {};
          return (
               <Flex style={{ flex: 1 }}>
                    <Image
                         source={{ uri: picture.large }}
                         resizeMode="contain"
                         style={{ flex: 1 }}
                    />
                    <View style={{ flex: 1, padding: 12 }}>
                         <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                              {`Tên: ${name.first} ${name.last}`}
                         </Text>

                         <Text style={{ fontSize: 17, marginTop: 10 }}>
                              {`Giới tính: ${gender.toUpperCase()}`}
                         </Text>
                         <Text
                              style={{
                                   fontSize: 17,
                                   marginTop: 10,
                              }}>
                              {`Số điện thoại: ${phone}`}
                         </Text>
                         <Text
                              style={{
                                   fontSize: 15,
                                   marginTop: 10,
                                   color: 'gray',
                              }}>{`Email: ${email}`}</Text>
                    </View>
               </Flex>
          );
     }
}

export default Detail;
