/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, SafeAreaView } from 'react-native';

//height: 50, width: 50
export default class FlexBox extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1, justifyContent: 'space-between' }}>
                <View
                    style={{
                        //flex: 1,
                        justifyContent: 'space-between',
                        //alignItems: 'flex-start',
                        flexDirection: 'row',
                    }}>
                    <View
                        style={{
                            //flex: 1,,
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                            //alignSelf: 'flex-start',
                        }}
                    />
                    <View
                        style={{
                            //flex: 1,
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                            //alignSelf: 'flex-start',
                        }}
                    />
                    <View
                        style={{
                            //flex: 1,
                            height: 50,
                            width: 50,
                            backgroundColor: 'blue',
                            //alignSelf: 'flex-end',
                        }}
                    />
                </View>

                <View
                    style={{
                        //flex: 1,
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                        //alignItems: 'flex-end',
                    }}>
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                            //alignSelf: 'flex-start',
                        }}
                    />
                    <View
                        style={{
                            //flex: 1,
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                            //alignSelf: 'flex-start',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'blue',
                            //alignSelf: 'flex-end',
                        }}
                    />
                </View>

                <View
                    style={{
                        //flex: 1,
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                        //alignItems: 'flex-end',
                    }}>
                    <View
                        style={{
                            //flex: 1,
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                            //alignSelf: 'flex-start',
                        }}
                    />
                    <View
                        style={{
                            //flex: 1,
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                            //alignSelf: 'flex-start',
                        }}
                    />
                    <View
                        style={{
                            //flex: 1,
                            height: 50,
                            width: 50,
                            backgroundColor: 'blue',
                            //alignSelf: 'flex-end',
                        }}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

//TODO Question 1:

/* <SafeAreaView style={{ flex: 1 }}>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'space-between',
                        alignItems: 'flex-end',
                        flexDirection: 'row',
                    }}>
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'yellow',
                        }}
                    />
                </View>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'flex-end',
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                    }}>
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'yellow',
                        }}
                    />
                </View>
            </SafeAreaView> */

//TODO Question 2:

/* <SafeAreaView style={{ flex: 1 }}>
                <View
                    style={{
                        flex: 1,
                        //alignItems: 'flex-end',
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                    }}>
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'blue',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'pink',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'green',
                        }}
                    />
                </View>

                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                    }}>
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'purple',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'orange',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'green',
                        }}
                    />
                </View>

                <View
                    style={{
                        flex: 1,
                        alignItems: 'flex-end',
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                    }}>
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'gray',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'yellow',
                        }}
                    />
                </View>
            </SafeAreaView> */

//TODO Question 3:
/* <SafeAreaView style={{ flex: 1, flexDirection: 'row' }}>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'blue',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'pink',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'green',
                        }}
                    />
                </View>

                <View
                    style={{
                        flex: 1,
                        alignItems: 'flex-end',
                        justifyContent: 'center',
                    }}>
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'red',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'gray',
                        }}
                    />
                    <View
                        style={{
                            height: 50,
                            width: 50,
                            backgroundColor: 'yellow',
                        }}
                    />
                </View>
            </SafeAreaView> */
