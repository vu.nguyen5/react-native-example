/* eslint-disable react-native/no-inline-styles */
import React, { Component, PureComponent } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import OldLifeCycle from './OldLifeCycle';
import NewLifeCycle from './NewLifeCycle';

export default class LifeCycle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'Please change title',
            isShowTitle: false,
        };
    }

    // static getDerivedStateFromProps(nextProps, prevState) {
    //     console.log(
    //         'Component---GET DERIVED STATE FROM PROPS---LifeCycle',
    //         nextProps,
    //         prevState,
    //     );
    //     return null;
    // }

    // shouldComponentUpdate(newProps, newState) {
    //     console.log(
    //         'Componet---SHOULD COMPONENT UPDATE---LifeCycle',
    //         newProps,
    //         newState,
    //     );
    //     return true;
    // }

    // UNSAFE_componentWillUpdate(nextProps, nextState) {
    //     console.log(
    //         'Component---WILL UPDATE---LifeCycle',
    //         nextProps,
    //         nextState,
    //     );
    // }

    // componentDidUpdate(prevProps, prevState) {
    //     console.log('Component---DID UPDATE---LifeCycle', prevProps, prevState);
    // }

    setTitle = () => {
        this.setState({ title: 'Welcome to MoMo' });
    };

    toggleTitle = () => {
        this.setState(prevState => ({
            isShowTitle: !prevState.isShowTitle,
        }));
    };

    render() {
        const { title, isShowTitle } = this.state;
        return (
            <View style={styles.container}>
                {/* {isShowTitle && <OldLifeCycle text={title} />} */}
                <Text style={styles.text}>{title}</Text>
                {/* {isShowTitle && <NewLifeCycle text={title} />} */}
                <TouchableOpacity onPress={this.setTitle} style={styles.button}>
                    <Text style={styles.btnText}>Change Title</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={this.toggleTitle}
                    style={styles.button}>
                    <Text style={styles.btnText}>
                        {isShowTitle ? 'Remove Title' : 'Show Title'}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    btnText: { fontSize: 18, color: 'white' },
    text: { fontSize: 18, color: '#b0006d' },
    button: {
        backgroundColor: '#b0006d',
        height: 50,
        width: 300,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
