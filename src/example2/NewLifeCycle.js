/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text } from 'react-native';

export default class NewLifeCycle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
        };
        console.log('Component---CONTRUCTOR---NewLifeCycle');
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        console.log(
            'Component---GET DERIVED STATE FROM PROPS---NewLifeCycle',
            nextProps,
            prevState,
        );
        // if (nextProps.text !== prevState.title) {
        //     return {
        //         title: nextProps.text,
        //     };
        // }
        return null;
    }

    componentDidMount() {
        console.log('Component---DID MOUNT---NewLifeCycle');
    }

    shouldComponentUpdate(newProps, newState) {
        console.log(
            'Componet---SHOULD COMPONENT UPDATE---NewLifeCycle',
            newProps,
            newState,
        );
        return true;
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log(
            'Component---WILL UPDATE---NewLifeCycle',
            prevProps,
            prevState,
        );
        // Are we adding new items to the list?
        // Capture the scroll position so we can adjust scroll later.
        // if (prevProps.list.length < this.props.list.length) {
        //     const list = this.listRef.current;
        //     return list.scrollHeight - list.scrollTop;
        // }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log(
            'Component---DID UPDATE---NewLifeCycle',
            prevProps,
            prevState,
            snapshot,
        );
        // If we have a snapshot value, we've just added new items.
        // Adjust scroll so these new items don't push the old ones out of view.
        // (snapshot here is the value returned from getSnapshotBeforeUpdate)
        // if (snapshot !== null) {
        //     const list = this.listRef.current;
        //     list.scrollTop = list.scrollHeight - snapshot;
        // }
    }

    componentWillUnmount() {
        console.log('Component---WILL UNMOUNT---NewLifeCycle');
    }

    render() {
        const { title } = this.state;
        const { text } = this.props;
        return (
            <Text style={{ fontSize: 25, color: '#b0006d' }}>
                {title ? title : 'Welcome to React Native'}
            </Text>
        );
    }
}
