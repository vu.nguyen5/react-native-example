/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text } from 'react-native';

export default class OldLifeCycle extends Component {
    constructor(props) {
        super(props);
        console.log('Component---CONTRUCTOR---OldLifeCycle');
    }

    UNSAFE_componentWillMount() {
        console.log('Component---WILL MOUNT---OldLifeCycle');
    }

    componentDidMount() {
        console.log('Component---DID MOUNT---OldLifeCycle');
    }

    UNSAFE_componentWillReceiveProps(newProps) {
        console.log('Component---WILL RECIEVE PROPS---OldLifeCycle', newProps);
    }

    shouldComponentUpdate(newProps, newState) {
        console.log(
            'Componet---SHOULD COMPONENT UPDATE---OldLifeCycle',
            newProps,
            newState,
        );
        return true;
    }

    UNSAFE_componentWillUpdate(nextProps, nextState) {
        console.log(
            'Component---WILL UPDATE---OldLifeCycle',
            nextProps,
            nextState,
        );
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(
            'Component---DID UPDATE---OldLifeCycle',
            prevProps,
            prevState,
        );
    }

    componentWillUnmount() {
        console.log('Component---WILL UNMOUNT---OldLifeCycle');
    }

    render() {
        const { text } = this.props;
        return (
            <Text style={{ fontSize: 25, color: '#b0006d' }}>
                {text ? text : 'Welcome to React Native'}
            </Text>
        );
    }
}
