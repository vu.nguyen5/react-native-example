/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

const StateLessComponent = ({ text }) => (
    <Text style={{ fontSize: 25, color: '#b0006d' }}>{text}</Text>
);

export default class StateFulComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'Welcome to React Native',
        };
    }

    setTitle = () => {
        this.setState({ title: 'Welcome to MoMo' });
    };

    render() {
        const { title } = this.state;
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                <TouchableOpacity
                    onPress={this.setTitle}
                    style={{
                        backgroundColor: '#b0006d',
                        height: 50,
                        width: 300,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginVertical: 10,
                    }}>
                    <Text style={{ fontSize: 18, color: 'white' }}>
                        Change Title
                    </Text>
                </TouchableOpacity>

                <StateLessComponent text={title} />

                <StateLessComponent text="Welcome to Momo" />
            </View>
        );
    }
}
