import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';

const sum = (a, b) => a + b;

const App = () => {
     const [isLoading, setLoading] = useState(false);
     const [isError, setError] = useState(false);
     const [data, setData] = useState([]);
     const [page, setPage] = useState(1);
     useEffect(() => {
          // Fetch api
          const fetchData = () => {
               const url = `https://randomuser.me/api/?seed=10&page=${page}&results=5`;
               fetch(url)
                    .then(res => res.json())
                    .then(res => setData(prev => [...prev, ...res.results]));
          };
          fetchData();
          //Did mount
          return () => {
               //unMount
          };
     }, [page]);

     // useEffect(() => {
     //   //Did update, do something whne isWarning change
     // }, [page]);

     const onLoadMore = () => setPage(prev => prev + 1);

     console.log('render', data);

     return (
          <View style={{ flex: 1, marginTop: 50 }}>
               <ScrollView
                    contentContainerStyle={{
                         alignItems: 'center',
                         justifyContent: 'center',
                    }}>
                    <TouchableOpacity onPress={onLoadMore}>
                         <Text>Change page</Text>
                    </TouchableOpacity>

                    <Text style={{ color: 'red', marginTop: 25 }}>{page}</Text>

                    <Text style={{ color: 'blue', marginTop: 25 }}>{`Length : ${
                         data.length
                    }`}</Text>

                    {data.map(item => (
                         <Text
                              key={item.email}
                              style={{ color: 'blue', marginTop: 25 }}>
                              {item.email}
                         </Text>
                    ))}
               </ScrollView>
          </View>
     );
};
export default App;
