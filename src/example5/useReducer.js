import React, { useReducer } from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';

const initialState = { count: 0 };

function reducer(state, action) {
     switch (action.type) {
          case 'increment':
               return { count: state.count + 1 };
          case 'decrement':
               return { count: state.count - 1 };
          default:
               throw new Error();
     }
}

function App() {
     const [state, dispatch] = useReducer(reducer, initialState);
     return (
          <>
               <Text style={{ color: 'red', marginTop: 25 }}>
                    {state.count}
               </Text>
               <TouchableOpacity
                    onPress={() => dispatch({ type: 'decrement' })}>
                    <Text>-</Text>
               </TouchableOpacity>
               <TouchableOpacity
                    onPress={() => dispatch({ type: 'increment' })}>
                    <Text>+</Text>
               </TouchableOpacity>
          </>
     );
}

export default App;
