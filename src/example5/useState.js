import React, { useState, useCallback, useMemo } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

const sum = (a, b) => a + b;

const App = () => {
     const [isWarning, setWarning] = useState(false);
     //const memorizeValue = useMemo(() => isWarning ? 10 : 0, []);
     //const Non_memorizeValue = isWarning ? 10 : 0
     //const [obj, setObj] = useState({id: 1, name: 'A'});

     const [intal, setIntal] = useState(() => {
          const data = sum(5, 5);
          return data;
     });

     const onChangeColor = () => {
          setWarning(!isWarning);
          //setWarning((prev) => !prev)
          //setObj({...obj, name: 'B'})
     };

     const setColor = useCallback(() => {
          setWarning(prev => !prev);
     }, []);
     //const setColor = useCallback(() => {setWarning(!isWarning}, []);

     return (
          <View
               style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
               }}>
               <TouchableOpacity onPress={setColor}>
                    <Text>Change light</Text>
               </TouchableOpacity>

               <Text
                    style={{
                         color: isWarning ? 'red' : 'blue',
                         marginTop: 25,
                    }}>{`Text color is ${isWarning ? 'red' : 'blue'}`}</Text>
               <Text
                    style={{
                         color: isWarning ? 'red' : 'blue',
                         marginTop: 25,
                    }}>
                    {intal}
               </Text>
          </View>
     );
};
export default App;
