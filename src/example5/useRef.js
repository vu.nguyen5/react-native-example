import React, {
     useState,
     useRef,
     forwardRef,
     useImperativeHandle,
} from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

const App = () => {
     const textRef = useRef(null);

     const onChangeTextRef = () =>
          textRef.current && textRef.current.onChangeText('Text change');
     return (
          <View
               style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
               }}>
               <TouchableOpacity onPress={onChangeTextRef}>
                    <Text>Change light</Text>
               </TouchableOpacity>
               <TextView ref={textRef} />
          </View>
     );
};

const TextView = forwardRef((props, ref) => {
     const [text, setText] = useState('false');

     useImperativeHandle(() => {
          onChangeText;
     }, ref);

     const onChangeText = value => setText(value);

     return (
          <View
               style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
               }}>
               <Text
                    style={{
                         color: 'blue',
                         marginTop: 25,
                    }}>
                    {text}
               </Text>
          </View>
     );
});
export default App;
