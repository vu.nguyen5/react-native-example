import React, { useContext } from 'react';
import { Text, View } from 'react-native';

const themes = {
     light: {
          foreground: '#000000',
          background: '#eeeeee',
     },
     dark: {
          foreground: '#ffffff',
          background: '#222222',
     },,
};

const ThemeContext = React.createContext(themes.light);

function App() {
     return (
          <ThemeContext.Provider value={themes.dark}>
               <Toolbar />
          </ThemeContext.Provider>
     );
}

function Toolbar(props) {
     return (
          <View
               style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
               }}>
               <ThemedButton />
          </View>
     );
}

function ThemedButton() {
     const theme = useContext(ThemeContext);
     return (
          <Text style={{ color: 'red', marginTop: 25 }}>{`${
               theme.background
          } - ${theme.foreground}`}</Text>
     );
}

export default App;
