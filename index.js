/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import Exmaple1 from './src/example1';
import Exmaple2 from './src/example2';
import Exmaple3 from './src/example3';
import Exmaple4 from './src/example4';

import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => Exmaple4);
